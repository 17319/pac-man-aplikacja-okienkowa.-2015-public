﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace IOProjekt_PacMan
{
    class Plansza
    {
        Random generatorLosowy = new Random();

        private Int32 szerokosc;
        public Int32 Szerokosc
        {
            get { return szerokosc; }
        }
        private Int32 wysokosc;
        public Int32 Wysokosc
        {
            get { return wysokosc; }
        }
        private Int32 pozycjaX;
        public Int32 PozycjaX
        {
            get { return pozycjaX; }
        }
        private Int32 pozycjaY;
        public Int32 PozycjaY
        {
            get { return pozycjaY; }
        }
        private Kierunek k;

        private TypPola[,] mapa;
        public TypPola[,] Mapa
        {
            get { return mapa; }
        }

        private Int32 punkty;
        public Int32 Punkty
        {
            get { return punkty; }
        }

        private List<Duch> listaDuchow;
        public List<Duch> ListaDuchow
        {
            get { return listaDuchow; }
        }

        Boolean czyKoniec;
        public Plansza(int szerokosc, int wysokosc)
        {
            this.szerokosc = szerokosc;
            this.wysokosc = wysokosc;
            this.pozycjaX = szerokosc / 2;
            this.pozycjaY = wysokosc / 2;
            this.punkty = 0;
            this.czyKoniec = false;
            mapa = new TypPola[this.szerokosc, this.wysokosc];
            for (int i = 0; i < this.szerokosc; i++)
            {
                for (int j = 0; j < this.wysokosc; j++)
                {
                    if (i == 0 || j == 0 || i == this.szerokosc - 1 || j == this.wysokosc - 1)
                    {
                        mapa[i, j] = TypPola.sciana;
                    }
                    else
                    {
                        mapa[i, j] = TypPola.kropka;
                    }
                }
            }

            listaDuchow = new List<Duch>();
            listaDuchow.Add(new Duch( new Point(1,1), Color.Green));
            listaDuchow.Add(new Duch(new Point(this.szerokosc-2, this.wysokosc-2), Color.Orange));

            generujMonety();
        }

        private void generujMonety()
        {
            Int32 x = 4;
            while (x > 0)
            {
                int i= generatorLosowy.Next(this.szerokosc);
                int j=generatorLosowy.Next(this.wysokosc);
                if(mapa[i,j ]==TypPola.kropka)
                {
                   mapa[i,j ] = TypPola.moneta;
                   x--;
                }
            }

        }

        internal void Graj()
        {
            if (!czyKoniec)
            {
                #region przesuwanie
                switch (k)
                {
                    case Kierunek.Gora:
                        if (mapa[pozycjaX, pozycjaY - 1] != TypPola.sciana)
                        {
                            pozycjaY--;
                        }
                        break;
                    case Kierunek.Dol:
                        if (mapa[pozycjaX, pozycjaY + 1] != TypPola.sciana)
                        {
                            pozycjaY++;
                        }
                        break;
                    case Kierunek.Prawo:
                        if (mapa[pozycjaX + 1, pozycjaY] != TypPola.sciana)
                        {
                            pozycjaX++;
                        }
                        break;
                    case Kierunek.Lewo:
                        if (mapa[pozycjaX - 1, pozycjaY] != TypPola.sciana)
                        {
                            pozycjaX--;
                        }
                        break;
                }
                #endregion

                #region punktacja
                if (mapa[pozycjaX, pozycjaY] == TypPola.kropka)
                {
                    punkty++;
                    mapa[pozycjaX, pozycjaY] = TypPola.puste;
                }
                else if (mapa[pozycjaX, pozycjaY] == TypPola.moneta)
                {
                    punkty += 100;
                    mapa[pozycjaX, pozycjaY] = TypPola.puste;
                }
                #endregion

                #region ruch duchów
                foreach (Duch d in listaDuchow)
                {
                    //rych ducha z prawdopodobienstwem 1/4
                    if (generatorLosowy.Next(4) == 0)
                    {
                        //czy ruch pionowo
                        if (Math.Abs(pozycjaX - d.Pozycja.X) < Math.Abs(pozycjaY - d.Pozycja.Y))
                        {
                            if (pozycjaY < d.Pozycja.Y)
                            {
                                d.Pozycja = new Point(d.Pozycja.X, d.Pozycja.Y - 1);
                            }
                            else
                            {
                                d.Pozycja = new Point(d.Pozycja.X, d.Pozycja.Y + 1);
                            }
                        }
                        //poziomo
                        else
                        {
                            if (pozycjaX < d.Pozycja.X)
                            {
                                d.Pozycja = new Point(d.Pozycja.X - 1, d.Pozycja.Y);
                            }
                            else
                            {
                                d.Pozycja = new Point(d.Pozycja.X + 1, d.Pozycja.Y);
                            }
                        }
                    }
                }
                #endregion

                sprawdzKolizje();
            }
        }

        private void sprawdzKolizje()
        {
            foreach (Duch d in listaDuchow)
            {
                if (d.Pozycja.X == pozycjaX && d.Pozycja.Y == pozycjaY)
                {
                    czyKoniec = true;
                    MessageBox.Show("Przegrałeś");
                }
            }
        }

        internal void ZmienKierunek(Kierunek kierunek)
        {
            k = kierunek;
        }
    }
}
