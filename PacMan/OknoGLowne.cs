﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace IOProjekt_PacMan
{
    public partial class OknoGLowne : Form
    {
        Plansza p;
        Wizualizacja wT, wG, wG2;
        public OknoGLowne()
        {
            InitializeComponent();
            p = new Plansza(25, 10);
            wT = new WizualizacjaTekstowa(p, widokTekstowy);
            wG = new WizualizacjaGraficzna(p, widokGraficzny);
            wG2 = new WizualizacjaGraficzna(p, pictureBox1);
            Odrysuj();
        }

        private void Odrysuj()
        {
            wT.Odrysuj();
            wG.Odrysuj();
            wG2.Odrysuj();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            p.Graj();
            Odrysuj();
        }

        private void OknoGLowne_KeyDown(object sender, KeyEventArgs e)
        {
            switch(e.KeyCode)
            {
                case Keys.Up: p.ZmienKierunek(Kierunek.Gora); break;
                case Keys.Down: p.ZmienKierunek(Kierunek.Dol); break;
                case Keys.Left: p.ZmienKierunek(Kierunek.Lewo); break;
                case Keys.Right: p.ZmienKierunek(Kierunek.Prawo); break;
                case Keys.Space: timer.Enabled = !timer.Enabled; break;
            }
            if (timer.Interval == 500 && 
                (e.KeyCode == Keys.Up ||e.KeyCode == Keys.Down || e.KeyCode == Keys.Left || e.KeyCode == Keys.Right))
            {
                timer.Interval = 100;
            }
        }

        private void OknoGLowne_KeyUp(object sender, KeyEventArgs e)
        {
            if (timer.Interval == 100 &&
                (e.KeyCode == Keys.Up || e.KeyCode == Keys.Down || e.KeyCode == Keys.Left || e.KeyCode == Keys.Right))
            {
                timer.Interval = 500;
            }
        }
    }
}
