﻿namespace IOProjekt_PacMan
{
    partial class OknoGLowne
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.widokTekstowy = new System.Windows.Forms.RichTextBox();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.widokGraficzny = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.widokGraficzny)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // widokTekstowy
            // 
            this.widokTekstowy.Enabled = false;
            this.widokTekstowy.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.widokTekstowy.Location = new System.Drawing.Point(13, 13);
            this.widokTekstowy.Name = "widokTekstowy";
            this.widokTekstowy.Size = new System.Drawing.Size(543, 227);
            this.widokTekstowy.TabIndex = 0;
            this.widokTekstowy.Text = "";
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 500;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // widokGraficzny
            // 
            this.widokGraficzny.Location = new System.Drawing.Point(13, 247);
            this.widokGraficzny.Name = "widokGraficzny";
            this.widokGraficzny.Size = new System.Drawing.Size(113, 188);
            this.widokGraficzny.TabIndex = 1;
            this.widokGraficzny.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(148, 247);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(420, 223);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // OknoGLowne
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(657, 482);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.widokGraficzny);
            this.Controls.Add(this.widokTekstowy);
            this.Name = "OknoGLowne";
            this.Text = "PacMan";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OknoGLowne_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.OknoGLowne_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.widokGraficzny)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox widokTekstowy;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.PictureBox widokGraficzny;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

