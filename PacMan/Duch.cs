﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace IOProjekt_PacMan
{

    class Duch
    {
        private Point pozycja;

        public Point Pozycja
        {
            get { return pozycja; }
            set { pozycja = value; }
        }
        private Color kolor;

        public Color Kolor
        {
            get { return kolor; }
            //set { kolor = value; }
        }

        public Duch(Point pozycja, Color kolor)
        {
            this.pozycja = pozycja;
            this.kolor = kolor;
        }
    }
}
