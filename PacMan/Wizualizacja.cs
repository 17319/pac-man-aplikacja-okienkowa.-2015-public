﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace IOProjekt_PacMan
{
    abstract class Wizualizacja
    {
        protected Plansza p;
        protected Wizualizacja(Plansza p)
        {
            this.p = p;
        }
        abstract public void Odrysuj();
    }

    class WizualizacjaTekstowa : Wizualizacja
    {
        private System.Windows.Forms.RichTextBox widokTekstowy;

        public WizualizacjaTekstowa(Plansza p, System.Windows.Forms.RichTextBox widokTekstowy)
            :base(p)
        {
            this.widokTekstowy = widokTekstowy;
        }

        public override void Odrysuj()
        {
            widokTekstowy.Clear();
            widokTekstowy.AppendText("Punkty: " + p.Punkty+"\n");
            for (int y = 0; y < p.Wysokosc; y++)
            {
                for (int x = 0; x < p.Szerokosc; x++)
                {
                    if (p.PozycjaX == x && p.PozycjaY == y)
                    {
                        widokTekstowy.AppendText("@");
                    }
                    else if(p.ListaDuchow.Count(d=>d.Pozycja==new Point(x,y))>0)
                    {
                        widokTekstowy.AppendText("$");
                    }
                    else if(p.Mapa[x,y]==TypPola.sciana)
                    {
                        widokTekstowy.AppendText("#");
                    }
                    else if (p.Mapa[x, y] == TypPola.moneta)
                    {
                        widokTekstowy.AppendText("O");
                    }
                    else if (p.Mapa[x, y] == TypPola.kropka)
                    {
                        widokTekstowy.AppendText(".");
                    }
                    else if (p.Mapa[x, y] == TypPola.puste)
                    {
                        widokTekstowy.AppendText(" ");
                    }
                    widokTekstowy.AppendText(" ");
                }
                widokTekstowy.AppendText("\n");
            }
        }
    }

    class WizualizacjaGraficzna : Wizualizacja
    {
        private System.Windows.Forms.PictureBox widokGraficzny;
        Graphics grafika;
        float szerokoscPola, wysokoscPola;
        public WizualizacjaGraficzna(Plansza p, System.Windows.Forms.PictureBox widokGraficzny)
            :base(p)
        {
            this.widokGraficzny = widokGraficzny;
            widokGraficzny.Image = new Bitmap(widokGraficzny.Width, widokGraficzny.Height);
            grafika = Graphics.FromImage(widokGraficzny.Image);
            szerokoscPola = widokGraficzny.Width / p.Szerokosc;
            wysokoscPola = widokGraficzny.Height / p.Wysokosc;
        }

        public override void Odrysuj()
        {
            grafika.Clear(Color.LightBlue);
            for (int y = 0; y < p.Wysokosc; y++)
            {
                for (int x = 0; x < p.Szerokosc; x++)
                {
                    if (p.Mapa[x, y] == TypPola.sciana)
                    {
                        grafika.FillRectangle(new SolidBrush(Color.Brown),
                                x * szerokoscPola,
                                y * wysokoscPola,
                                szerokoscPola,
                                wysokoscPola);
                    }
                    else if (p.Mapa[x, y] == TypPola.moneta)
                    {
                        grafika.FillEllipse(new SolidBrush(Color.Red),
                                x * szerokoscPola,
                                y * wysokoscPola,
                                szerokoscPola,
                                wysokoscPola);
                    }
                    else if (p.Mapa[x, y] == TypPola.kropka)
                    {
                        grafika.FillEllipse(new SolidBrush(Color.White),
                                x * szerokoscPola+szerokoscPola/3,
                                y * wysokoscPola + wysokoscPola/3,
                                Math.Max( szerokoscPola/4,2),
                                Math.Max( wysokoscPola / 4, 2));
                    }
                }
            }
            grafika.FillEllipse(new SolidBrush(Color.Yellow),
                                p.PozycjaX * szerokoscPola,
                                p.PozycjaY * wysokoscPola,
                                szerokoscPola,
                                wysokoscPola);

            grafika.DrawString("Punkty: " + p.Punkty,
                               new Font(new FontFamily("Arial"), wysokoscPola, FontStyle.Regular, GraphicsUnit.Pixel), 
                               new SolidBrush(Color.White), 
                               new Point(0, 0));

            foreach (Duch d in p.ListaDuchow)
            {
                grafika.FillEllipse(new SolidBrush(d.Kolor),
                                d.Pozycja.X * szerokoscPola,
                                d.Pozycja.Y * wysokoscPola,
                                szerokoscPola,
                                wysokoscPola);
            }
            widokGraficzny.Refresh();
        }
    }
}
